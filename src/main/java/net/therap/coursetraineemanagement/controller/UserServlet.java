package net.therap.coursetraineemanagement.controller;

import net.therap.coursetraineemanagement.domain.Admin;
import net.therap.coursetraineemanagement.domain.Course;
import net.therap.coursetraineemanagement.domain.Trainee;
import net.therap.coursetraineemanagement.domain.User;
import net.therap.coursetraineemanagement.helper.HashGeneration;
import net.therap.coursetraineemanagement.service.AdminService;
import net.therap.coursetraineemanagement.service.EnrollmentService;
import net.therap.coursetraineemanagement.service.TraineeService;
import net.therap.coursetraineemanagement.service.UserService;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;

/**
 * @author soumik.sarker
 * @since 9/7/21
 */
public class UserServlet extends HttpServlet {
}
